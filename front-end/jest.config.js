module.exports = {
    clearMocks: true,
    collectCoverageFrom: ['app/**/*.{js,jsx,mjs,tsx,ts}'],
    moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx'],
    setupFiles: [
        '<rootDir>/config/enzyme.config.js',
        '<rootDir>/config/setupJest.ts'
    ],
    testEnvironment: 'jsdom',
    testMatch: ['**/__tests__/**/*.ts?(x)', '**/?(*.)+(spec|test).ts?(x)'],
    testPathIgnorePatterns: ['\\\\node_modules\\\\'],
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
    verbose: false,
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/app/__mocks__/fileMock.js',
        '\\.(scss|css|less)$': '<rootDir>/app/__mocks__/styleMock.js'
    }
};