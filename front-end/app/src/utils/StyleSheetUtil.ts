export const createCSSRule = (
    sheet: StyleSheetList,
    selectorToFind: string,
    selectorToCreate: string,
    rules: string,
    index?: number
): void =>  {
    const cssSheetIndex = getStylesheetIndexForRule(sheet, selectorToFind);
    if (!cssSheetIndex) {
        return;
    }

    const cssSheet = sheet[cssSheetIndex] as CSSStyleSheet;
    if('addRule' in cssSheet) {
        cssSheet.addRule(selectorToCreate, rules, index);
    }
}

const getStylesheetIndexForRule = (sheet: StyleSheetList, selector: string):  number | null => {
    for (let i:number = 0; i < sheet.length; i++) {
        const rules = (sheet.item(i) as CSSStyleSheet).cssRules;
        for (let j = 0; j < rules.length; j++) {
            const rule = (rules.item(j) as CSSStyleRule);
            if (rule.selectorText === selector) {
                return i;
            }
        }
    }

    return null;
};


