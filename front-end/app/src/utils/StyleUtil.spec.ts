import {getHorizAlignment, getSize, getVertAlignment} from './StyleUtil';
import {AlignEnum} from '../enums/align';
import {SizeEnum} from '../enums/size';


describe ('Style uitl', () => {

    describe('Horiz Alignement', () => {
        it ('should return center alignment if one is not provided', () => {
            expect(getHorizAlignment()).toBe('horizAlignCenter');
        });

        it ('should return correct alignment if given enum pertains to x axis', () => {
            expect(getHorizAlignment(AlignEnum.RIGHT)).toBe('horizAlignRight');
        });

        it ('should return center alignment if given enum pertains to y axis', () => {
            expect(getHorizAlignment(AlignEnum.BOTTOM)).toBe('horizAlignCenter');
        });
    });

    describe('Vertical Alignment', () => {
        it ('should return center alignment if one is not provided', () => {
            expect(getVertAlignment()).toBe('vertAlignCenter');
        });

        it ('should return center alignment if given enum pertains to x axis', () => {
            expect(getVertAlignment(AlignEnum.RIGHT)).toBe('vertAlignCenter');
        });

        it ('should return correct alignment if given enum pertains to y axis', () => {
            expect(getVertAlignment(AlignEnum.BOTTOM)).toBe('vertAlignBottom');
        });
    });

    describe('Size', function () {
        it ('should return medium size if one is not provided', () => {
            expect(getSize()).toBe('medium');
        });

        it ('should return correct size for the provided enum', () => {
            expect(getSize(SizeEnum.LARGE)).toBe('large');
        });

        it ('should return default if the size provided is not valid', () => {
            expect(getSize(SizeEnum.XLARGE)).toBe('medium');
        });
    });
})