import {AlignEnum} from '../enums/align';
import {SizeEnum} from '../enums/size';

export const getHorizAlignment = (alignment?: AlignEnum): string => {
    switch (alignment) {
        case AlignEnum.LEFT:
            return ' ';
        case AlignEnum.RIGHT:
            return 'horizAlignRight';
        case AlignEnum.CENTER:
            return 'horizAlignCenter';
        default:
            return 'horizAlignCenter';
    }
}

export const getVertAlignment = (alignment?: AlignEnum): string => {
    switch (alignment) {
        case AlignEnum.TOP:
            return 'vertAlignTop';
        case AlignEnum.CENTER:
            return 'vertAlignCenter';
        case AlignEnum.BOTTOM:
            return 'vertAlignBottom';
        default:
            return 'vertAlignCenter';
    }
}

export const getSize = (size?: SizeEnum): string => {
    switch (size) {
        case SizeEnum.LARGE:
            return 'large';
        case SizeEnum.MEDIUM:
            return 'medium';
        case SizeEnum.SMALL:
            return 'small';
        default:
            return 'medium';
    }
}