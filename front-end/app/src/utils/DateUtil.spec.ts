import {getMonthFromDate} from './DateUtil';


describe('Date Util', () => {
    it('Should get the month string from date', () => {
        const date = new Date(2019, 5);
        expect(getMonthFromDate(date)).toBe('May');
    });
});