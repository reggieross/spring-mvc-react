export enum AlignEnum {
    LEFT,
    RIGHT,
    CENTER,
    TOP,
    BOTTOM,
}