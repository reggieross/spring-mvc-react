export enum SizeEnum {
    XSMALL,
    SMALL,
    MEDIUM,
    LARGE,
    XLARGE,
}