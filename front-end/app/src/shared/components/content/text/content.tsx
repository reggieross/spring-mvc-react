import React from 'react';
import {SizeEnum} from '../../../../enums/size';

import classNames from 'classnames';
import {getSize} from '../../../../utils/StyleUtil';

interface ContentProps {
  size?: SizeEnum;
  children: React.ReactNode;

}

const Content = (props: ContentProps): any => {
  const classes = classNames(
    getSize(props.size),
    'content-root',
  );

  return (
    <div className={classes}>
      {props.children}
    </div>
  );
};

export default Content;
