// @ts-ignore
import React from 'react';
import {mount} from 'enzyme';
import {SizeEnum} from '../../../../enums/size';
import Header from './header';

describe('Header component', () => {
    it('Should render the approiate header level when given a size', () => {
        const reactWrapper = mount(<Header size={SizeEnum.XLARGE}> I am a child </Header>);
        expect(reactWrapper.find('h1').exists()).toBe(true);
    });

    it('Should render h3  if no header is provided', () => {
        const reactWrapper = mount(<Header> I am a child </Header>);
        expect(reactWrapper.find('h3').exists()).toBe(true);
    });
});
