import React  from 'react';
import {SizeEnum} from '../../../../enums/size';

interface HeaderProps {
  size?: SizeEnum;
  children: React.ReactNode;
}


const Header = (props: HeaderProps): any => (
  <div className={'header-root'}>
    {getHeaderLevel(props.size, props.children)}
  </div>
);

const getHeaderLevel = (size: SizeEnum, children: React.ReactNode): React.ReactNode => {
  switch (size) {
    case SizeEnum.XLARGE:
      return <h1>{children}</h1>;
    case SizeEnum.LARGE:
      return <h2>{children} </h2>;
    case SizeEnum.MEDIUM:
      return <h3>{children}</h3>;
    case SizeEnum.SMALL:
      return <h4>{children}</h4>;
    case SizeEnum.XSMALL:
      return <h5>{children}</h5>;
    default:
      return <h3>{children}</h3>;
  }
};

export default Header;
