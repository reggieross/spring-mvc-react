import React from 'react';
import Link from './link';
import classNames from 'classnames';
require('./tabStyleLink.module.scss');

interface TabStyleLinkProps {
    id: string;
    href: string;
    children: React.ReactNode;
    selectedId?: string;
    onClick?: (event: any) => void;
}

export class TabStyleLink extends React.Component<TabStyleLinkProps, {}> {
    public render(): React.ReactNode {
        return (
            <Link href={this.props.href}>
                <div
                    id={this.props.id}
                    onClick={this.props.onClick}
                    className={classNames(
                    'tabStyleLink-root',
                    {'tabStyleLink-selected': this.props.selectedId === this.props.id}
                )}>
                    {this.props.children}
                </div>
            </Link>
        );
    }
}
