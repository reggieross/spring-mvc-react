import React from 'react';
import './link.module.scss';

interface LinkProps {
    href: string;
    id?: string;
    label?: string;
    onClick?: (...props: any) => any;
    children: React.ReactNode;
    style?: any;
}
const Link = (props: LinkProps): any => {
    const container =
        <div
            className={'link-root'}
            style={props.style}
            aria-label={props.label}
            id={props.id}
            onClick={props.onClick}>
            {props.children}
        </div>;

    return props.onClick
        ? container
        : (<a className={'link-root'} href={props.href}>{container}</a>);
}

export default Link;
