import React  from 'react';
import classNames from 'classnames';
import {getHorizAlignment, getVertAlignment} from '../../../utils/StyleUtil';
import {AlignEnum} from '../../../enums/align';
import {createCSSRule} from '../../../utils/StyleSheetUtil';
require('./row.module.scss');

interface RowProps {
    colSpan?: number,
    offset?: number,
    horizAlign?: AlignEnum,
    vertAlign?: AlignEnum,
    className?: any,
    children: React.ReactNode
}

const Row = (props: RowProps): any => {
    //TODO: This is not quite viable becuase we are going to generate a new class for each row
    // There is still a possibility for classes to clash
    const randomRowClass = `row-position-offset-${getHash()}`;

    const rowClasses = classNames(
        'row-root',
        'row-position',
        randomRowClass,
        getVertAlignment(props.vertAlign),
        getHorizAlignment(props.horizAlign),
        props.className
    );

    const {colSpan, offset} = props;
    if (colSpan) {
        createCSSRule(
            document.styleSheets,
            '.row-root',
            `.${randomRowClass}`,
            `grid-column: ${offset ? offset : 'auto'} / span ${colSpan}`
        );
    }

    return (
      <div className={rowClasses}>
        {props.children}
      </div>
    );
}

const getHash = (): string => {
    return  Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15);
}

export default Row;