import React from 'react';
import classNames from 'classnames';
require('./grid.module.scss');

interface GridProps {
    children: React.ReactNode;
    className?: string
}
export const Grid = (props: GridProps): any => (
    <div className={classNames('grid-root', props.className)} >
        {props.children}
    </div>
);

export default Grid;
