import React from 'react';
import './drawers.module.scss';

interface DrawersProps {
    children: React.ReactNode;
    className?: string;
    title: string;
}

interface DrawersState {
    open: boolean;
}

export class Drawers extends React.Component<DrawersProps, DrawersState> {
    constructor(props: DrawersProps) {
        super(props);
        this.state = {
            open: false,
        };
    }

    public render(): any {
        const drawerContent =
            <div className={'drawers-content'}>
                {this.props.children}
            </div>;

        return (
            <div className={'drawers-root'} >
                <div className={'drawers-title'} aria-label={this.props.title} onClick={this.onClick}>
                    {this.props.title}
                </div>
                {this.state.open && drawerContent}
            </div>
        );
    }

    private onClick = (): void => {
        this.setState({
            open: !this.state.open
        });
    }
}

export default Drawers;
