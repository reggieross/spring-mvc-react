import React from 'react';
import './drawer.module.scss';

interface DrawersProps {
    children: React.ReactNode;
    id?: string;
    onClick?: (event: any) => void;
}
export const Drawer = (props: DrawersProps): any =>  (
    <div
        id={props.id}
        className={'drawer-root'}
        onClick={props.onClick}
    >
        {props.children}
    </div>

);

export default Drawer;
