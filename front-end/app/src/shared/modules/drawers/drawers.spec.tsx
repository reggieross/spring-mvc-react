import { mount } from 'enzyme';
import React from 'react';
import Drawers from './drawers';

describe('drawers module', () => {
    it('Should display the drawer items when clickeed', () => {
        const wrapper = mount(
            <Drawers title={'some-title'}>
                <div aria-label={'drawer-item'}>Some Item</div>
            </Drawers>
        );
        expect(wrapper.find('[aria-label="drawer-item"]').at(0).exists()).toBe(false);
        wrapper.find('[aria-label="some-title"]').at(0).simulate('click');
        expect(wrapper.find('[aria-label="drawer-item"]').at(0).exists()).toBe(true);
    });
});