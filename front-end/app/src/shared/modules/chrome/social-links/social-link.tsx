import React from 'react';
import {Platform} from '../../../../enums/platform';
import Link from '../../../components/content/links/link';


interface SocialLinkProps {
    platform: Platform;
    iconColor?: string;
    href: string;
}

export const getIconForPlatform = (platform: Platform): any  => {
    switch (platform) {
        case Platform.FACEBOOK:
            return <i aria-label={'fb-icon'} className={'icon fab fa-facebook-square'} />;
        case Platform.INSTAGRAM:
            return <i aria-label={'ig-icon'} className={'icon fab fa-instagram'} />;
        case Platform.YOUTUBE:
            return <i aria-label={'youtube-icon'} className={'icon fab fa-youtube'} />;
        case Platform.TWITTER:
            return <i aria-label={'twitter-icon'} className={'icon fab fa-twitter-square'} />;
        default:
            return null;
    }
};

const SocialLink = (props: SocialLinkProps): any => {
    const iconStyles = {
        color : props.iconColor,
    };

    return (
        <Link style={iconStyles} href={props.href}>
            {getIconForPlatform(props.platform)}
        </Link>
    );
};

export default SocialLink;
