import { mount } from 'enzyme';
import React from 'react';
import {Platform} from '../../../../enums/platform';
import SocialLink from './social-link';


describe('social link', () => {
    it('should show correct icon for platform', () => {
        const reactWrapper = mount(
            <SocialLink
                platform={Platform.TWITTER}
                href={'test'}
            />,
        );

        expect(reactWrapper.find('i[aria-label=\'twitter-icon\']').exists()).toBe(true);
    });
});
