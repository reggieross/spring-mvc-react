import React from 'react';
import {TabStyleLink} from '../../../components/content/links/tabStyleLink';
require('./nav.module.scss');

export const HOME_LINK_ID = 'home';
export const ABOUT_LINK_ID = 'about';
export const GUIDE_LINK_ID = 'guide';

interface NavProps {
    selectedId: string;
}

export class Nav extends React.Component<NavProps> {
    public render(): any {
        const {
            selectedId
        } = this.props;
        return (
            <div className={'nav-root'}>
                <div>Place Holder for nav implementation</div>
            </div>
        );
    }
}