import React from 'react';
import {Nav} from './nav/nav';
import {Footer} from './footer/footer';
import Grid from '../../components/grid/grid';
require('./chrome.module.scss');

interface ChromeProps {
    selectedId: string
    children: React.ReactNode;
}

class Chrome extends React.Component<ChromeProps> {
    public render(): any {
        return (
            <div>
                <Nav selectedId={this.props.selectedId} />
                <Grid className={'chrome-page-content'}>
                    {this.props.children}
                </Grid>
                <Footer />
            </div>
        );
    }
}

export default Chrome;