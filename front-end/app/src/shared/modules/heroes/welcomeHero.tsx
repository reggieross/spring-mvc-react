import React from 'react';
import Header from '../../components/content/text/header';
import {SizeEnum} from '../../../enums/size';
import Content from '../../components/content/text/content';
require('./welcomeHero.module.scss');


export const WelcomeHero = (_: {}): any => {
 return (
     <div className={'welcomeHero-root '}>
         <div>
             <Header size={SizeEnum.XLARGE}>
                 WELCOME!
             </Header>
             <Content size={SizeEnum.MEDIUM}>
                 A brain dump by Reginald Ross
             </Content>
         </div>
     </div>
 );
}