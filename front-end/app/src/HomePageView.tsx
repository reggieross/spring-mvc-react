import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Chrome from './shared/modules/chrome/chrome';
import Row from './shared/components/grid/row';
import {HOME_LINK_ID} from './shared/modules/chrome/nav/nav';
import {WelcomeHero} from './shared/modules/heroes/welcomeHero';
import {Service} from './services/domain/Service';

interface HomePageViewState {
    responseObjects: ResponseObject[];
}

class HomePageView extends Component<{}, HomePageViewState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            responseObjects: []
        }
    }

    async componentDidMount(): Promise<void> {
        const products = await Service.getResponseObjects();
        this.setState({
            responseObjects: products
        });
    }

    public render(): any {
        return (
            <Chrome selectedId={HOME_LINK_ID}>
                <Row colSpan={12} offset={0}>
                    <WelcomeHero />
                </Row>
                <Row colSpan={6} offset={0}>
                    {JSON.stringify(this.state.responseObjects)}
                </Row>
                <Row colSpan={6} offset={0}>
                    This is a test of the Row
                </Row>
            </Chrome>
        );
    }
}

ReactDOM.render(<HomePageView />, document.getElementById('root'));
