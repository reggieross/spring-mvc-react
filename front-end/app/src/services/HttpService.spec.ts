import {HttpService, ResponseEntity} from './HttpService';
import fetch from 'jest-fetch-mock'


describe('HttpService', () => {
    describe('GET', () => {
        beforeEach(() => {
            fetchMock.resetMocks()
        })

        it('Should Return a response entity with status code and json object', async () => {
            const expectedResponse = {
                statusCode: 200,
                responseObj: {
                    someField: 'test',
                }
            } as ResponseEntity;

            fetchMock.mockResponse(JSON.stringify(expectedResponse.responseObj));

            const response = await HttpService.get('/some/url');
            expect(response).toEqual(expectedResponse);
        });

        it('Should send get with correct headers', async () => {
            const expectedResponse = {
                statusCode: 200,
                responseObj: {
                    someField: 'test',
                }
            } as ResponseEntity;

            fetchMock.mockResponse(JSON.stringify(expectedResponse.responseObj));

            await HttpService.get('/some/url');
            expect(fetchMock).toBeCalledWith(
                '/some/url',
                {'headers': {'Content-Type': 'application/json'}, 'method': 'GET'}
            );
        });
    });

    describe('POST', () => {
        beforeEach(() => {
            fetchMock.resetMocks()
        })

        it('Should Return a response entity with status code and json object', async () => {
            const expectedResponse = {
                statusCode: 200,
                responseObj: {
                    someField: 'test',
                }
            } as ResponseEntity;

            fetchMock.mockResponse(JSON.stringify(expectedResponse.responseObj));

            const response = await HttpService.post('/some/url', {data: 'this is a test'});
            expect(response).toEqual(expectedResponse);
        });

        it('Should send get with correct headers', async () => {

            const expectedResponse = {
                statusCode: 200,
                responseObj: {
                    someField: 'test',
                }
            } as ResponseEntity;

            fetchMock.mockResponse(JSON.stringify(expectedResponse.responseObj));

            const body = {data: 'this is a test'};

            await HttpService.post('/some/url', body);
            expect(fetchMock).toBeCalledWith(
                '/some/url',
                {
                    'headers': {'Content-Type': 'application/json'},
                    'method': 'POST',
                    'body': '{"data":"this is a test"}',
                }
            );
        });
    });


});