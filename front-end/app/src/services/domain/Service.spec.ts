import {HttpService} from '../HttpService';
import {Service} from './Service';

describe('Service', () =>  {
    it ('Should get all domain', async () => {
      const spy = jest.spyOn(HttpService, 'get');
      const mockResp = {
          statusCode: 200,
          responseObj: [{name: 'product1'}, {name: 'product2'}]
      };
      spy.mockResolvedValue(mockResp);

      const response = await Service.getResponseObjects();
      expect(response).toEqual(mockResp.responseObj);
    })
});