import {HttpService} from '../HttpService';

export class Service {
    public static async getResponseObjects(): Promise<ResponseObject[]> {
        try {
            const resp = await HttpService.get('/api');
            return resp.responseObj;
        } catch (e) {
            return [];
        }
    }
}