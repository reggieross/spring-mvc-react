package rest.domain.stub;

import entities.ResponseObject;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import rest.domain.IClient;

import java.util.Arrays;
import java.util.List;

@Profile("stub")
@Service("client")
public class ClientStub implements IClient {

    @Override
    public ResponseEntity<List<ResponseObject>> getResponseObjects() {
        List<ResponseObject> body = Arrays.asList(
                ResponseObject.builder().name("some-name").build(),
                ResponseObject.builder().name("some-name-2").build(),
                ResponseObject.builder().name("some-name-3").build(),
                ResponseObject.builder().name("some-name-4").build()
        );

        return new ResponseEntity<>(body, HttpStatus.OK);
    }
}
