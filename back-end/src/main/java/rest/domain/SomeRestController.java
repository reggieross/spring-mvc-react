package rest.domain;

import entities.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class SomeRestController {

    private IClient client;

    public SomeRestController(@Autowired IClient client) {
        this.client = client;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ResponseObject> getResponseObject() {
        ResponseEntity<List<ResponseObject>> response = client.getResponseObjects();
        return response.getBody();
    }
}
