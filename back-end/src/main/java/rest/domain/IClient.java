package rest.domain;

import entities.ResponseObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "${productApi.name}", url = "${productApi.url}")
@Profile("!stub")
public interface IClient {

    @RequestMapping(method = RequestMethod.GET, path = "/products", consumes = "application/json")
    ResponseEntity<List<ResponseObject>> getResponseObjects();
}
