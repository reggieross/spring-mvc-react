package entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
@JsonDeserialize(builder = ResponseObject.ProductBuilder.class)
public class ResponseObject {
    private String name;

    @JsonPOJOBuilder(withPrefix = "")
    public static class ProductBuilder {}
}
