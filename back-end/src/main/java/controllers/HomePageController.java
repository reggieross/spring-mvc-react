package controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@CrossOrigin(origins = "*")
@RestController
public class HomePageController {

    @RequestMapping("/")
    public ModelAndView handleRequest () {
        return new ModelAndView("index");
    }
}
